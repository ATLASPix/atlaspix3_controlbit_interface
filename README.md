# ATLASPix3 Control bit interface


This simple IP interface a few special control bit that are driven by pads on the ATLASPix3 ASIC to AXI registers . Refer to ATLASPix3 manual for explanation on the use of such control pins. 


**register 0** (0x0), INVERT, 1 bit 
**register 1** (0x4), INTERFACE_SPEED, 1 bit
**register 2** (0x8), ALWAYS_ENB, 1 bit
**register 3** (0xC), TAKEFAST, 1 bit

